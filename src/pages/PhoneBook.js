import React, {useState} from 'react';
import PersonForm from "../components/PersonForm";
import Persons from "../components/Persons";
import Filter from "../components/Filter";

const PhoneBook = () => {
    const [persons, setPersons] = useState([
        {name: 'Arto Hellas', number: '951155879'}
    ])
    const [newName, setNewName] = useState('')
    const [newNumber, setNewNumber] = useState('')
    const [searchPerson, setSearchPerson] = useState('')

    let filter = persons

    const handleClick = () => {
        const newPerson = {
            name: newName,
            number: newNumber
        }

        setPersons([
            ...persons,
            newPerson,
        ])

        setNewName("")
        setNewNumber("")
    }

    const handleChangePersonName = (event) => {
        const valueName = event.target.value
        setNewName(valueName)
    }

    const handleChangePersonNumber = (event) => {
        const valueNumber = event.target.value
        setNewNumber(valueNumber)
    }

    if (searchPerson.length > 0) {
        filter = persons.filter((person) => {
            return person.name.includes(searchPerson)
        })
    }

    const handleSearch = (event) => {
        const valueSearch = event.target.value
        setSearchPerson(valueSearch)

    }

    const handleSubmit = (event) => {
        event.preventDefault()
    }

    return (
        <div>
            <h2>Phonebook</h2>
            <Filter handleSearch={handleSearch}/>
            <h2>Add a new</h2>
            <PersonForm
                handleSubmit={handleSubmit}
                handleChangePersonName={handleChangePersonName}
                newName={newName}
                handleChangePersonNumber={handleChangePersonNumber}
                newNumber={newNumber}
                handleClick={handleClick}/>
            <h2>Numbers</h2>
            <Persons filter={filter} newName={newName}/>
        </div>
    )
};

export default PhoneBook;
