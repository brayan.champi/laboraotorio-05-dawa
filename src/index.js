import React from 'react'
import ReactDOM from 'react-dom'
import PhoneBook from "./pages/PhoneBook";

const App = () => {
    return <PhoneBook />
}

ReactDOM.render(<App />, document.getElementById('root'))
