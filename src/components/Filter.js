import React from 'react';

const Filter = (props) => {
    const {handleSearch} = props
    return (
        <div>
            filter shown with <input onChange={handleSearch} type="text" placeholder="Search..."/>
        </div>
    )
}

export default Filter
